import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { VendorAppIntegrationModule } from '@empaia/vendor-app-integration';

import { AppComponent } from './app.component';
import { VendorAppSurfaceComponent } from './vendor-app-surface/vendor-app-surface.component';
import { environment } from '../environments/environment';
import { MaterialModule } from './material/material.module';
import { DataCreatorComponent } from './data-creator/data-creator.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { VendorAppSurfaceListComponent } from './vendor-app-surface-list/vendor-app-surface-list.component';

@NgModule({
  declarations: [
    AppComponent,
    VendorAppSurfaceComponent,
    DataCreatorComponent,
    VendorAppSurfaceListComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    VendorAppIntegrationModule,
    MaterialModule,
    ReactiveFormsModule,
  ],
  providers: [
    { provide: 'APP_URL_ONE', useValue: environment.appUrlOne },
    { provide: 'APP_URL_TWO', useValue: environment.appUrlTwo },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
