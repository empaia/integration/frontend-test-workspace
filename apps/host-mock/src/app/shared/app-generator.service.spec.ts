import { TestBed } from '@angular/core/testing';

import { AppGeneratorService } from './app-generator.service';

describe('AppGeneratorService', () => {
  let service: AppGeneratorService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: 'APP_URL_ONE', useValue: 'APP_ONE_URL' },
        { provide: 'APP_URL_TWO', useValue: 'APP_TWO_URL' }
      ]
    });
    service = TestBed.inject(AppGeneratorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
