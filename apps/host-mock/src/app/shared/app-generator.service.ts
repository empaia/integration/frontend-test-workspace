import { Inject, Injectable } from '@angular/core';
import { App } from './app.models';

@Injectable({
  providedIn: 'root'
})
export class AppGeneratorService {

  public apps: App[] = [];
  public sandboxParameters = 'allow-scripts';

  constructor(
    @Inject('APP_URL_ONE') private appUrlOne: string,
    @Inject('APP_URL_TWO') private appUrlTwo: string,
  ) {
    this.apps = [
      {
        id: 'TEST_APP_ONE',
        appName: 'Angular App',
        appUrl: this.appUrlOne,
      },
      {
        id: 'TEST_APP_TWO',
        appName: 'Plain JavaScript App',
        appUrl: this.appUrlTwo
      }
    ];
  }
}
