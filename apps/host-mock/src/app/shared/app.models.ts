export interface App {
  id: string;
  appName: string;
  appUrl: string;
}
