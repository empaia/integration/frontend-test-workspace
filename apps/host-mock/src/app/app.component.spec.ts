import { TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { VendorAppSurfaceComponent } from './vendor-app-surface/vendor-app-surface.component';
import { DataCreatorComponent } from './data-creator/data-creator.component';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { VendorAppIntegrationModule } from '@empaia/vendor-app-integration';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AppComponent, VendorAppSurfaceComponent, DataCreatorComponent],
      providers: [
        { provide: 'APP_URL_ONE', useValue: 'AppOne' },
        { provide: 'APP_URL_TWO', useValue: 'AppTwo' },
        FormBuilder,
      ],
      imports: [
        MaterialModule,
        BrowserAnimationsModule,
        VendorAppIntegrationModule,
        ReactiveFormsModule,
      ]
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'host-mock'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('host-mock');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('h1')?.textContent).toContain(
      'App Selection'
    );
  });
});
