import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AppGeneratorService } from '../shared/app-generator.service';
import { MatRadioChange } from '@angular/material/radio';
import { Scope, Token, WbsUrl } from '@empaia/vendor-app-integration';

@Component({
  selector: 'app-vendor-app-surface-list',
  templateUrl: './vendor-app-surface-list.component.html',
  styleUrls: ['./vendor-app-surface-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VendorAppSurfaceListComponent {
  public currentAppId = '';
  public currentAppUrl = '';

  public scopeMap = new Map<string, Scope>();
  public tokenMap = new Map<string, Token>();
  public wbsUrlMap = new Map<string, WbsUrl>();
  public scopeReadyMap = new Map<string, boolean>();
  public tokenReadyMap = new Map<string, boolean>();
  public wbsUrlReadyMap = new Map<string, boolean>();
  public tokenRequestMap = new Map<string, boolean>();

  constructor(
    public appGeneratorService: AppGeneratorService,
    ) { }

  public onRadioButtonChange(event: MatRadioChange): void {
    this.currentAppId = event.value.id;
    this.currentAppUrl = event.value.appUrl;
    this.tokenRequestMap.set(event.value.id, false);
  }

  public onReceiveScopeReady(event: string): void {
    if (event) {
      this.scopeReadyMap.set(event, true);
    }
  }

  public onReceivedTokenReady(event: string): void {
    if (event) {
      this.tokenReadyMap.set(event, true);
    }
  }

  public onReceivedWbsUrlReady(event: string): void {
    if (event) {
      this.wbsUrlReadyMap.set(event, true);
    }
  }

  public onReceivedTokenRequest(event: string): void {
    if (event) {
      this.tokenRequestMap.set(event, true);
    }
  }

  public sendScope(text: string): void {
    this.scopeMap.set(this.currentAppId, { id: text, type: 'scope' });
  }

  public sendToken(text: string): void {
    this.tokenMap.set(this.currentAppId, { value: text, type: 'token' });
  }

  public sendWbsUrl(text: string): void {
    this.wbsUrlMap.set(this.currentAppId, { url: text, type: 'wbsUrl' });
  }
}
