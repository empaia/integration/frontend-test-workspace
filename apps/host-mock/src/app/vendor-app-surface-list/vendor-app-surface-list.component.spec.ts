import { ComponentFixture, TestBed } from '@angular/core/testing';
import { VendorAppSurfaceListComponent } from './vendor-app-surface-list.component';

describe('VendorAppSurfaceListComponent', () => {
  let component: VendorAppSurfaceListComponent;
  let fixture: ComponentFixture<VendorAppSurfaceListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VendorAppSurfaceListComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(VendorAppSurfaceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
