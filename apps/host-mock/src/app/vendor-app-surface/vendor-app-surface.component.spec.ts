import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorAppSurfaceComponent } from './vendor-app-surface.component';
import { AppGeneratorService } from '../shared/app-generator.service';
import { DataCreatorComponent } from '../data-creator/data-creator.component';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { VendorAppIntegrationModule } from '@empaia/vendor-app-integration';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('VendorAppSurfaceComponent', () => {
  let component: VendorAppSurfaceComponent;
  let fixture: ComponentFixture<VendorAppSurfaceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VendorAppSurfaceComponent, DataCreatorComponent ],
      providers: [
        AppGeneratorService,
        FormBuilder,
        { provide: 'APP_URL_ONE', useValue: 'APP_ONE_URL' },
        { provide: 'APP_URL_TWO', useValue: 'APP_TWO_URL' }
      ],
      imports: [
        BrowserAnimationsModule,
        MaterialModule,
        VendorAppIntegrationModule,
        ReactiveFormsModule,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorAppSurfaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
