import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { Request, Scope, ScopeReady, Token, TokenReady, WbsUrl, WbsUrlReady } from '@empaia/vendor-app-integration';

@Component({
  selector: 'app-vendor-app-surface',
  templateUrl: './vendor-app-surface.component.html',
  styleUrls: ['./vendor-app-surface.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VendorAppSurfaceComponent {
  @Input() public appUrl!: string;
  @Input() public appId!: string;
  @Input() public isActive!: boolean;
  @Input() public sandboxParameters!: string;
  @Input() public scope!: Scope;
  @Input() public token!: Token;
  @Input() public wbsUrl!: WbsUrl;

  @Output() public receiveScopeReady = new EventEmitter<string>();
  @Output() public receiveTokenReady = new EventEmitter<string>();
  @Output() public receiveWbsUrlReady = new EventEmitter<string>();
  @Output() public receiveTokenRequest = new EventEmitter<string>();

  public onReceiveScopeReady(scopeReady: ScopeReady): void {
    if (scopeReady) {
      this.receiveScopeReady.emit(this.appId);
    }
  }

  public onReceiveTokenReady(tokenReady: TokenReady): void {
    if (tokenReady) {
      this.receiveTokenReady.emit(this.appId);
    }
  }

  public onReceiveWbsUrlReady(wbsUrlReady: WbsUrlReady): void {
    if (wbsUrlReady) {
      this.receiveWbsUrlReady.emit(this.appId);
    }
  }

  public onReceiveTokenRequest(tokenRequest: Request): void {
    if (tokenRequest) {
      this.receiveTokenRequest.emit(this.appId);
    }
  }
}
