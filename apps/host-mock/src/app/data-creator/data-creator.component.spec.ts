import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataCreatorComponent } from './data-creator.component';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('DataCreatorComponent', () => {
  let component: DataCreatorComponent;
  let fixture: ComponentFixture<DataCreatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataCreatorComponent ],
      providers: [
        FormBuilder
      ],
      imports: [
        BrowserAnimationsModule,
        MaterialModule,
        ReactiveFormsModule,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataCreatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
