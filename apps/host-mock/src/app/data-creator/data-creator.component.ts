import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-data-creator',
  templateUrl: './data-creator.component.html',
  styleUrls: ['./data-creator.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DataCreatorComponent implements OnInit {
  dataForm!: FormGroup;
  @Input() labelText!: string;
  @Output() sendData = new EventEmitter<string>();

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.dataForm = this.fb.group({
      text: ['', Validators.required],
    });
  }


  submitData(): void {
    const data = this.dataForm.value.text;
    this.sendData.emit(data);
    this.dataForm.reset();
  }

  resetForm(): void {
    this.dataForm.reset();
  }
}
