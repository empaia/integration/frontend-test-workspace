import { Page, expect, test } from '@playwright/test';

test.describe.parallel('Host Mock', () => {
  const APP_ONE = '#embeddedApp-TEST_APP_ONE';
  const APP_TWO = '#embeddedApp-TEST_APP_TWO';

  const scope = 'Test Scope Object';
  const token = 'Test Token Object';
  const wbsUrl = 'Test WbsUrl Object';

  test('should start an app and marked as ready', async ({ page }) => {
    await page.goto('http://localhost:4200');
    await page.locator('[data-cy=radio-buttons]', { hasText: 'Angular App' }).click();
    expect(page.frameLocator('iframe')).toBeTruthy();
    await expect(page.locator('[data-cy=scope-ready]')).toHaveText(/Is Scope ready: true/);
    await expect(page.locator('[data-cy=token-ready]')).toHaveText(/Is Token ready: true/);
    await expect(page.locator('[data-cy=wbs-url-ready]')).toHaveText(/Is WbsUrl ready: true/);
  });

  test.describe.parallel('Angular App', () => {
    test.beforeEach(async ({ page }) => {
      await page.goto('http://localhost:4200');
      await page.locator('[data-cy=radio-buttons]', { hasText: 'Angular App' }).click();
    });

    test('should send an scope object', async ({ page }) => {
      await sendScope(page, scope);
      await expect(page.frameLocator(APP_ONE).locator('[data-cy=received-scope]').locator('[data-cy=received-data]')).toHaveText(scope);
    });

    test('should send an token object', async ({ page }) => {
      await sendToken(page, token);
      await expect(page.frameLocator(APP_ONE).locator('[data-cy=received-token]').locator('[data-cy=received-data]')).toHaveText(token);
    });

    test('should send an wbs url object', async ({ page }) => {
      await sendWbsUrl(page, wbsUrl);
      await expect(page.frameLocator(APP_ONE).locator('[data-cy=received-wbs-url]').locator('[data-cy=received-data]')).toHaveText(wbsUrl);
    });

    test('should receive a new token request', async ({ page }) => {
      await sendToken(page, 'Test');
      await expect(page.locator('[data-cy=token-request]')).toHaveText(/Received: false/);
      await page.frameLocator(APP_ONE).locator('[data-cy=request-new-token]').click();
      await expect(page.locator('[data-cy=token-request]')).toHaveText(/Received: true/);
    });
  });

  test.describe.parallel('Plan JavaScript App', () => {
    test.beforeEach(async ({ page }) => {
      await page.goto('http://localhost:4200');
      await page.locator('[data-cy=radio-buttons]', { hasText: 'Plain JavaScript App' }).click();
    });

    test('should send an scope object', async ({ page }) => {
      await sendScope(page, scope);
      await expect(page.frameLocator(APP_TWO).locator('[id=scope-data]')).toHaveText(scope);
    });

    test('should send an token object', async ({ page }) => {
      await sendToken(page, token);
      await expect(page.frameLocator(APP_TWO).locator('[id=token-data]')).toHaveText(token);
    });

    test('test send an wbs url object', async ({ page }) => {
      await sendWbsUrl(page, wbsUrl);
      await expect(page.frameLocator(APP_TWO).locator('[id=wbs-url-data]')).toHaveText(wbsUrl);
    });

    test('should receive a new token request', async ({ page }) => {
      await sendToken(page, 'Test');
      await expect(page.locator('[data-cy=token-request]')).toHaveText(/Received: false/);
      await page.frameLocator(APP_TWO).locator('[id=request-new-token]').click();
      await expect(page.locator('[data-cy=token-request]')).toHaveText(/Received: true/);
    });
  });
});

async function sendScope(page: Page, scope: string): Promise<void> {
  await expect(page.locator('[data-cy=scope-ready]')).toHaveText(/Is Scope ready: true/);
  await page.locator('[data-cy=scope-id-input]').locator('[data-cy=text-input]').type(scope);
  await page.locator('[data-cy=scope-id-input]').locator('[data-cy=send-text-button]').click();
}

async function sendToken(page: Page, token: string): Promise<void> {
  await expect(page.locator('[data-cy=token-ready]')).toHaveText(/Is Token ready: true/);
  await page.locator('[data-cy=token-input]').locator('[data-cy=text-input]').type(token);
  await page.locator('[data-cy=token-input]').locator('[data-cy=send-text-button]').click();
}

async function sendWbsUrl(page: Page, wbsUrl: string) {
  await expect(page.locator('[data-cy=wbs-url-ready]')).toHaveText(/Is WbsUrl ready: true/);
  await page.locator('[data-cy=wbs-url-input]').locator('[data-cy=text-input]').type(wbsUrl);
  await page.locator('[data-cy=wbs-url-input]').locator('[data-cy=send-text-button]').click();
}
