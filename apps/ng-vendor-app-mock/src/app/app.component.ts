import { Component, NgZone, OnInit } from '@angular/core';
import {
  Scope,
  WbsUrl,
  Token,
  addScopeListener,
  addTokenListener,
  addWbsUrlListener,
  requestNewToken
} from '@empaia/vendor-app-communication-interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'ng-vendor-app-mock';

  public scopeId!: string;
  public tokenValue!: string;
  public baseUrl!: string;

  constructor(private ngZone: NgZone) { }


  ngOnInit(): void {
    // give the scope listener a callback function with
    // a scope object as parameter
    addScopeListener((scope: Scope) => {
      this.ngZone.run(() => {
        this.scopeId = scope.id;
      });
    });

    // give the token listener a callback function with
    // a token object as parameter
    addTokenListener((token: Token) => {
      this.ngZone.run(() => {
        this.tokenValue = token.value;
      });
    });

    // give the wbsUrl listener a callback function with
    // a wbsUrl object as parameter
    addWbsUrlListener((wbsUrl: WbsUrl) => {
      this.ngZone.run(() => {
        this.baseUrl = wbsUrl.url;
      });
    });
  }

  onRequestTokenClick(): void {
    requestNewToken();
  }
}
