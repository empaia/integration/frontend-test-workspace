import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'app-data-display',
  templateUrl: './data-display.component.html',
  styleUrls: ['./data-display.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DataDisplayComponent {
  @Input() dataKind!: string;
  @Input() data!: string;
}
