function receivedData(elemId, data) {
  const dataInput = document.getElementById(elemId);
  dataInput.innerText = data;
}

VendorAppCommunicationInterface.addScopeListener(function(scope) {
  // the scope object contains the attribute id which is the scopeId
  const scopeId = scope.id;
  receivedData('scope-data', scopeId);
});

VendorAppCommunicationInterface.addTokenListener(function(token) {
  // the token object contains the attribute value which is the access token
  const accessToken = token.value;
  receivedData('token-data', accessToken);
});

VendorAppCommunicationInterface.addWbsUrlListener(function(wbsUrl) {
  // the wbsUrl object contains the attribute url which is the base url to the Workbench Service 2.0
  const url = wbsUrl.url;
  receivedData('wbs-url-data', url);
});
